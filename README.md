# Automated commit

A repo with an example for creating commit automatically in MR. More information in [the article](https://how-to.dev/how-to-create-commit-automatically-in-a-merge-request-in-gitlab)
